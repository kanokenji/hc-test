<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Part;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class LoadCategoriesAndPartsData extends Fixture
{
    protected $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager)
    {
        $aCategoryNames = array('First Category', 'Second Category', 'Third Category');
        foreach($aCategoryNames as $categoryName) {
            $category = new Category();
            $category->setCategoryName($categoryName);

            for($i = 1; $i < 5; $i++) {
                $part = new Part();
                $part->setPartName($this->faker->text(30));
                $manager->persist($part);

                $category->addPart($part);
            }

            $manager->persist($category);
        }

        $manager->flush();
    }
}
