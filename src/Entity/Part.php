<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PartRepository")
 * @ORM\Table(name="parts")
 */
class Part
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="parts")
     * @ORM\JoinColumn(name="id_category", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank()
     * @Assert\Type(type="Category")
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=150)
     * @Assert\NotBlank
     */
    private $partName;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Category|null
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @param Category $category
     *
     * @return self
     */
    public function setCategory(Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPartName(): ?string
    {
        return $this->partName;
    }

    /**
     * @param string $partName
     *
     * @return self
     */
    public function setPartName(string $partName): self
    {
        $this->partName = $partName;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->partName;
    }
}
