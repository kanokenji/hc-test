<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 * @ORM\Table(name="categories")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank
     */
    private $categoryName;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Part", mappedBy="category", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $parts;

    public function __construct()
    {
        $this->parts = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getCategoryName(): ?string
    {
        return $this->categoryName;
    }

    /**
     * @param string $categoryName
     *
     * @return self
     */
    public function setCategoryName(string $categoryName): self
    {
        $this->categoryName = $categoryName;

        return $this;
    }

    /**
     * @param Part $part
     *
     * @return self
     */
    public function addPart(Part $part) : self
    {
        if (!$this->parts->contains($part)) {
            $this->parts[] = $part;
            $part->setCategory($this);
        }

        return $this;
    }

    /**
     * @param Part $part
     *
     * @return self
     */
    public function removePart(Part $part) : self
    {
        if ($this->parts->contains($part)) {
            $this->parts->removeElement($part);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getParts(): array
    {
        return $this->parts->toArray();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->categoryName;
    }
}
