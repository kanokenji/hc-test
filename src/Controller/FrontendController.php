<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Part;
use App\Repository\CategoryRepository;
use App\Repository\PartRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FrontendController extends AbstractController
{
    /** @var CategoryRepository $categoryRepository */
    private $categoryRepository;

    /** @var PartRepository $partRepository */
    private $partRepository;

    public function __construct(CategoryRepository $categoryRepository, PartRepository $partRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->partRepository = $partRepository;
    }

    /**
     * @Route("/", name="home")
     */
    public function viewAll()
    {
        $categories = $this->categoryRepository->findAll();

        return $this->render('frontend/index.html.twig', [
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("/search", name="categories_search")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function search(Request $request)
    {
        $query = $request->query->get('q');
        $categories = $this->categoryRepository->searchByQuery($query);

        return $this->render('frontend/query_category.html.twig', [
            'categories' => $categories
        ]);
    }
}
